'''
graphs-adj-list.py
Graphs with explicit representation management
Group (4):

Costa, C. J. <cjcosta@utad.pt>
'''
def init_graph(edges, directed=False):
    adj_list = {} # or adj_list = dict()
    for u, v in edges:
        add_edge(adj_list, u,v, directed)

    return adj_list

def add_edge(graph, u, v, directed=False):
    if u not in graph:
        graph[u] = set()
    graph[u].add(v)
    if v not in graph:
        graph[v] = set()
    if not directed:
        graph[v].add(u)

def get_vertices(graph):
    return list(sorted(graph.keys()))

def get_edges(graph):
    return [(k,v) for k in graph.keys() for v in graph[k]]

def is_directed(graph):
    for u in graph:
        for v in graph[u]:
            if u not in graph[v]:
                return True
    return False 

        
def as_edge(graph, u, v):
    return u in graph and v in graph[u]


def delete_edge(graph):
    pass

def delete_vertice(graph):
    pass


def get_num_vertices(graph):
    pass

def get_num_edges(graph):
    pass

if __name__ == '__main__': # to test module
    import networkx as nx
    import matplotlib.pyplot as plt

    # Cria a lista de arestas.
    arestas = [('A', 'B'), ('B', 'C'), ('B', 'D'), ('C', 'B'), ('C', 'E'), ('D', 'A'), ('E', 'B')]

    grafo = init_graph(arestas, directed=True)
    print('     First:', arestas)
    print('     Grafo:', grafo)
    print('  Vertices:', get_vertices(grafo))
    print('   Arestas:', get_edges(grafo))
    print('É dirigido:', is_directed(grafo))
    print("Existe a aresta ('A','B')?", as_edge(grafo,'A','B'))
    print("Existe a aresta ('B','A')?", as_edge(grafo,'B','A'))
    G = None
    if is_directed(grafo) == True:
        G = nx.DiGraph()
    else:
        G = nx.Graph()
    G.add_edges_from(get_edges(grafo))
    nx.draw(G, with_labels = True)
    plt.show()
    print("Nova ('E','F') não dirigida:")
    add_edge(grafo,'E','F') 
    print('     Grafo:', grafo)
    print('  Vertices:', get_vertices(grafo))
    print('   Arestas:', get_edges(grafo))
    print('É dirigido:', is_directed(grafo))
    if is_directed(grafo) == True:
        G = nx.DiGraph()
    else:
        G = nx.Graph()
    G.add_edges_from(get_edges(grafo))
    nx.draw(G, with_labels = True)
    plt.show()
    print()

    arestas = [('A', 'E'), ('A', 'D'), ('E','F'), ('C', 'F'), ('C', 'B'), ('D', 'C'), ('B', 'F')]

    grafo = init_graph(arestas)
    print('     Second:', arestas)
    print('      Grafo:', grafo)
    print('   Vertices:', get_vertices(grafo))
    print('    Arestas:', get_edges(grafo))
    print('É dirigido:', is_directed(grafo))
    if is_directed(grafo) == True:
        G = nx.DiGraph()
    else:
        G = nx.Graph()
    G.add_edges_from(get_edges(grafo))
    nx.draw(G, with_labels = True)
    plt.show()
    add_edge(grafo,'F','G',True)
    print("Nova ('F','G') dirigida:")
    print('      Grafo:', grafo)
    print('   Vertices:', get_vertices(grafo))
    print('    Arestas:', get_edges(grafo))
    print('É dirigido:', is_directed(grafo))
    if is_directed(grafo) == True:
        G = nx.DiGraph()
    else:
        G = nx.Graph()
    G.add_edges_from(get_edges(grafo))
    nx.draw(G, with_labels = True)
    plt.show()
    print()
